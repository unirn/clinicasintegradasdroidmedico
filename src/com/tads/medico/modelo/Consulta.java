package com.tads.medico.modelo;

public class Consulta {
	private int id;
	private int id_consulta;
	private String paciente;
	private String dataConsulta;
	private String horarioConsulta;
	private String clinica;
	private String statusConsulta;
	
	//Constantes com o nome das colunas do banco
	public static final String ID = "_id";
	public static final String ID_CONSULTA = "id_consulta";
	public static final String PACIENTE = "paciente";
	public static final String DATA_CONSUTA = "dataConsulta";
	public static final String HORARIO_CONSULTA = "horarioConsulta";
	public static final String CLINICA = "clinica";
	public static final String STATUS_CONSULTA = "statusConsulta";
	
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId_consulta() {
		return id_consulta;
	}
	
	public void setId_consulta(int id_consulta) {
		this.id_consulta = id_consulta;
	}
	
	public String getPaciente() {
		return paciente;
	}
	
	public void setPaciente(String paciente) {
		this.paciente = paciente;
	}
	
	public String getDataConsulta() {
		return dataConsulta;
	}
	
	public void setDataConsulta(String dataConsulta) {
		this.dataConsulta = dataConsulta;
	}
	
	public String getHoraConsulta() {
		return horarioConsulta;
	}
	
	public void setHoraConsulta(String horaConsulta) {
		this.horarioConsulta = horaConsulta;
	}
	
	public String getClinica() {
		return clinica;
	}
	
	public void setClinica(String clinica) {
		this.clinica = clinica;
	}
	
	public String getStatusConsulta() {
		return statusConsulta;
	}
	
	public void setStatusConsulta(String statusConsulta) {
		this.statusConsulta = statusConsulta;
	}
}