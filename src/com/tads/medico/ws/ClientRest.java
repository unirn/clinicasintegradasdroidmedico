package com.tads.medico.ws;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.tads.medico.modelo.Consulta;

public class ClientRest {
	private String ip = "10.17.1.137:8080";
	
	public List<Consulta> getListaConsulta(String dataInicial, String dataFinal, int idClinica) throws Exception {
		String url = "http://" + ip + "/recepcao/service/buscarConsultaPerido/" +
				"?datainicio=" + dataInicial + 
				"&datafim=" + dataFinal + 
				"&idclinica=" + idClinica;
		
		Log.i("URL: ", url);
		
		String[] resposta = new WebServiceClient().get(url);
		List<Consulta> listaConsultas = new ArrayList<Consulta>();

		if (resposta[0].equals("200")) {
			Gson gson = new Gson();
			
			JsonParser parser = new JsonParser();
			JsonArray array = parser.parse(resposta[1]).getAsJsonArray();
			
			for (JsonElement jsonElement : array) {
				listaConsultas.add(gson.fromJson(jsonElement, Consulta.class));
			}
			
			return listaConsultas;
		} else {
			throw new Exception(resposta[1]);
		}
	}
	
	public List<Consulta> getListaMes() throws Exception {
		String url = "http://" + ip + "/recepcao/service/buscarConsultaMes/";
		
		Log.i("URL: ", url);
		
		String[] resposta = new WebServiceClient().get(url);
		List<Consulta> listaConsultas = new ArrayList<Consulta>();

		if (resposta[0].equals("200")) {
			Gson gson = new Gson();
			
			JsonParser parser = new JsonParser();
			JsonArray array = parser.parse(resposta[1]).getAsJsonArray();
			
			for (JsonElement jsonElement : array) {
				listaConsultas.add(gson.fromJson(jsonElement, Consulta.class));
			}
			
			return listaConsultas;
		} else {
			throw new Exception(resposta[1]);
		}
	}
}