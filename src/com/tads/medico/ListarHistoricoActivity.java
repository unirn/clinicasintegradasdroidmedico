package com.tads.medico;

import java.util.ArrayList;
import java.util.List;

import com.tads.medico.modelo.Consulta;
import com.tads.medico.banco.RepositorioConsulta;
import com.tads.medico.listview.AdapterListView;
import com.tads.medico.listview.ItemListView;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("HandlerLeak")
public class ListarHistoricoActivity extends Activity implements
		OnItemClickListener, Runnable {
	private List<Consulta> listaConsultas;
	private ArrayList<ItemListView> listInfoConsultas;
	private AdapterListView adapterListView;
	private ListView listView;
	private RepositorioConsulta repositorioConsulta;
	private ProgressDialog progressDialog;
	private Thread thread;
	private Button btLimparHistorico;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_listar_historico);

		listView = (ListView) findViewById(R.id.tela_consulta_listView);
		listView.setOnItemClickListener(this);

		btLimparHistorico = (Button) findViewById(R.id.btLimparHistorico);
		btLimparHistorico.setOnClickListener(btLimparHistoricoListener);

		repositorioConsulta = new RepositorioConsulta(ListarHistoricoActivity.this);

		progressDialog = ProgressDialog.show(ListarHistoricoActivity.this,
				"Aguarde", "Processando...");

		thread = new Thread(ListarHistoricoActivity.this);
		thread.start();
	}
	
	@Override
    public void onPause(){
        super.onPause();
        repositorioConsulta.close();

    }

	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Consulta c = listaConsultas.get(arg2);
		showCustomDialog(c);
	}

	private void showCustomDialog(Consulta c) {
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.custom_dialog);
		dialog.setTitle("Informações da consulta");

		TextView dialogPaciente = (TextView) dialog.findViewById(R.id.exibiNomeDialog);
		TextView dialogData = (TextView) dialog.findViewById(R.id.exibirDataDialog);
		TextView dialogHora = (TextView) dialog.findViewById(R.id.exibirHoraDialog);
		TextView dialogClinica = (TextView) dialog.findViewById(R.id.exibirClinicaDialog);
		TextView dialogStatus = (TextView) dialog.findViewById(R.id.exibirStatusDialog);

		dialogPaciente.setText(c.getPaciente());
		dialogData.setText(c.getDataConsulta());
		dialogHora.setText(c.getHoraConsulta());
		dialogClinica.setText(c.getClinica());
		dialogStatus.setText(c.getStatusConsulta());

		final Button ok = (Button) dialog.findViewById(R.id.bt_ok);

		ok.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	private OnClickListener btLimparHistoricoListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			exibirAlerta();
		}
	};

	@Override
	public void run() {
		int tipoClinica = -1;
		try {
			listaConsultas = repositorioConsulta.listarConsultas();

			listInfoConsultas = new ArrayList<ItemListView>();

			for (Consulta consulta : listaConsultas) {

				if (consulta.getClinica().equals("Psicologia")) {
					tipoClinica = 1;
				} else if (consulta.getClinica().equals("Fisioterapia")) {
					tipoClinica = 2;
				} else {
					tipoClinica = 1;
				}

				
				ItemListView itens = new ItemListView(
						tipoClinica,
						consulta.getPaciente(),
						consulta.getDataConsulta() + " - " + 
						consulta.getHoraConsulta(),
						consulta.getStatusConsulta()
				);

				listInfoConsultas.add(itens);
			}

			adapterListView = new AdapterListView(this, listInfoConsultas);
		} catch (Exception e) {
			e.printStackTrace();
		}

		handler.sendEmptyMessage(0);
	}

	private void exibirAlerta() {
		final RepositorioConsulta rc = new RepositorioConsulta(ListarHistoricoActivity.this);
		
		AlertDialog.Builder dialog = new AlertDialog.Builder(ListarHistoricoActivity.this);
		dialog.setMessage("Deseja excluir todas as consultas do seu histórico? \n(Essa ação não ira excluir as infomações no servidor.)");
	
		dialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface di, int arg) {
				int qtd = rc.deletarTodos();
				Toast.makeText(ListarHistoricoActivity.this, "Consultas excluidas: " + qtd, Toast.LENGTH_LONG).show();
				
				finish();
				startActivity(getIntent());
			}
		});

		dialog.setNegativeButton("Não", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface di, int arg) {
				
			}
		});
		
		dialog.setTitle("Aviso");
		dialog.show();
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			listView.setAdapter(adapterListView);
			progressDialog.dismiss();
		}
	};
}