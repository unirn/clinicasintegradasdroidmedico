package com.tads.medico;

import com.tads.medico.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;

public class MenuBuscarClinicas extends Activity {
	private ImageButton
		btClinicas,
		btFisioterapia,
		btPsicologia,
		btFarmacia,
		btNutricao,
		btEdFisica,
		btEnfermagem;

	private String[] listaClinicas = new String[] {"Selecione a clinica", "Todas as clinicas", "Fisioterapia", "Psicologia", 
			"Farmacia", "Nutrição", "Ed. Física", "Enfermagem" };
	
	private Spinner spinnerClinicas;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_buscar_clinicas);
        
        spinnerClinicas = (Spinner) findViewById(R.id.spinnerClinicas);
        spinnerClinicas.setOnItemSelectedListener(spinnerClinicasListener);
        preencherSpinnerClinicas();
        
        inicializarBotoes();
        ocultarBotoes();
        
        btFisioterapia.setOnClickListener(btFisioterapiaOnClickListener);
        btPsicologia.setOnClickListener(btPsicologiaOnClickListener);
        
        btClinicas.setOnClickListener(btClinicasOnClickListener);
    }
    
    private OnItemSelectedListener spinnerClinicasListener = new AdapterView.OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			if(arg2 == 0){
				ocultarBotoes();
			} else if(arg2 == 1){
				ocultarBotoes();
				btClinicas.setVisibility(View.VISIBLE);
			} else if(arg2 == 2){
				ocultarBotoes();
				btFisioterapia.setVisibility(View.VISIBLE);
			} else if(arg2 == 3){
				ocultarBotoes();
				btPsicologia.setVisibility(View.VISIBLE);
			} else if(arg2 == 4){
				ocultarBotoes();
				btFarmacia.setVisibility(View.VISIBLE);
			} else if(arg2 == 5){
				ocultarBotoes();
				btNutricao.setVisibility(View.VISIBLE);
			} else if(arg2 == 6){
				ocultarBotoes();
				btEdFisica.setVisibility(View.VISIBLE);
			} else if(arg2 == 7){
				ocultarBotoes();
				btEnfermagem.setVisibility(View.VISIBLE);
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) { }
	};
    
	private OnClickListener btClinicasOnClickListener = new OnClickListener() {
        public void onClick(View v) {
        	Intent it = new Intent(MenuBuscarClinicas.this, BuscarConsultaDataActivity.class);
			Bundle parametros = new Bundle();
			parametros.putInt("idClinica", 0);
			it.putExtras(parametros);
			startActivity(it);
        }
    };
    
    private OnClickListener btFisioterapiaOnClickListener = new OnClickListener() {
        public void onClick(View v) {
        	Intent it = new Intent(MenuBuscarClinicas.this, BuscarConsultaDataActivity.class);
			Bundle parametros = new Bundle();
			parametros.putInt("idClinica", 1);
			it.putExtras(parametros);
			startActivity(it);
        }
    };
    
    private OnClickListener btPsicologiaOnClickListener = new OnClickListener() {
        public void onClick(View v) {
        	Intent it = new Intent(MenuBuscarClinicas.this, BuscarConsultaDataActivity.class);
			Bundle parametros = new Bundle();
			parametros.putInt("idClinica", 2);
			it.putExtras(parametros);
			startActivity(it);
        }
    };
    
    private void inicializarBotoes(){
    	btClinicas = (ImageButton) findViewById(R.id.btClinicas);
    	btFisioterapia = (ImageButton) findViewById(R.id.btFisioterapia);
    	btPsicologia = (ImageButton) findViewById(R.id.btPsicologia);
		btFarmacia = (ImageButton) findViewById(R.id.btFarmacia);
		btNutricao = (ImageButton) findViewById(R.id.btNutricao);
		btEdFisica = (ImageButton) findViewById(R.id.btEdFisica);
		btEnfermagem = (ImageButton) findViewById(R.id.btEnfermagem);
    }
    
    private void ocultarBotoes(){
    	btClinicas.setVisibility(View.GONE);
    	btFisioterapia.setVisibility(View.GONE);
    	btPsicologia.setVisibility(View.GONE);
		btFarmacia.setVisibility(View.GONE);
		btNutricao.setVisibility(View.GONE);
		btEdFisica.setVisibility(View.GONE);
		btEnfermagem.setVisibility(View.GONE);
    }
    
    private void preencherSpinnerClinicas() {
		ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listaClinicas);
		adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerClinicas.setAdapter(adaptador);
	}
}