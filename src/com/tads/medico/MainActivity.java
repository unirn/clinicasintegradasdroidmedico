package com.tads.medico;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

	private SimpleDateFormat formato;
	private Date dataAtual;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		formato = new SimpleDateFormat("yyyy-MM-dd");
		dataAtual = new Date();

		Button btListarClinicas = (Button) findViewById(R.id.btListarClinicas);
		Button btListarDia = (Button) findViewById(R.id.btListarDia);
		Button btListarMes = (Button) findViewById(R.id.btListarMes);
		Button btListarSalvas = (Button) findViewById(R.id.btListarSalvas);

		btListarClinicas.setOnClickListener(btListarClinicasOnClickListener);
		btListarDia.setOnClickListener(btListarDiaOnClickListener);
		btListarMes.setOnClickListener(btListarMesOnClickListener);
		btListarSalvas.setOnClickListener(btListarSalvasOnClickListener);
	}

	private OnClickListener btListarClinicasOnClickListener = new OnClickListener() {
		public void onClick(View v) {
			Intent it = new Intent(MainActivity.this, MenuBuscarClinicas.class);
			Bundle parametros = new Bundle();
			parametros.putInt("idClinica", 0);
			it.putExtras(parametros);
			startActivity(it);
		}
	};

	private OnClickListener btListarDiaOnClickListener = new OnClickListener() {
		public void onClick(View v) {
			Intent it = new Intent(MainActivity.this, ListarActivity.class);
			Bundle parametros = new Bundle();

			parametros.putInt("idClinica", 0);
			parametros.putString("dataInicialTxt",formato.format(dataAtual));
			parametros.putString("dataFinalTxt", formato.format(dataAtual));

			it.putExtras(parametros);
			startActivity(it);
		}
	};

	private OnClickListener btListarMesOnClickListener = new OnClickListener() {
		public void onClick(View v) {
			Intent it = new Intent(MainActivity.this, ListarActivity.class);
			startActivity(it);
		}
	};

	private OnClickListener btListarSalvasOnClickListener = new OnClickListener() {
		public void onClick(View v) {
			Intent it = new Intent(MainActivity.this, ListarHistoricoActivity.class);
			startActivity(it);

		}
	};
}